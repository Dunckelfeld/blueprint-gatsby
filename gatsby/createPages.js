const path = require('path');
const fs = require('fs');

module.exports = async ({ actions, graphql }) => {
  const { createPage } = actions;

  // Query all pages from craft.
  const result = await graphql(`
    {
      craft {
        entries {
          id
          uri
          fullUri # note, that the fullUri is provided by our custom version of craftql
          title
          language
          section {
            handle
          }
          type {
            handle
          }
        }
      }
    }
  `);

  if (result.errors) {
    // eslint-disable-next-line no-console
    result.errors.forEach(e => console.error(e.toString()));
    return Promise.reject(result.errors);
  }

  // Grab the entries off the result.
  const { entries } = result.data.craft;

  /**
   * ==============================
   * Create pages for entries
   * ==============================
   */
  entries.forEach(({ id, uri, language, section, type }) => {
    // Determine corresponding template component.
    // See templates on advice about how to name your craft sections and entry types.
    const templateFile = path.resolve(`src/templates/${section.handle}.js`);

    // Determine target gatsby uri.
    let gatsbyUri = '';

    if (!uri) {
      console.warn(`Skipping ${section.handle}, no uri given by source.`);
      return;
    }

    switch (uri) {
      // map home entry to gatsby uri
      case '__home__':
        gatsbyUri = '/';
        break;

      // map 404 entry to gatsby uri
      case '404':
        gatsbyUri = '/404.html';
        break;

      // keep other entries as given
      default:
        gatsbyUri = `/${uri}`;
        break;
    }

    // Only process the entry if there is a template for it.
    // And if there is a usable uri.
    if (fs.existsSync(templateFile) && gatsbyUri.length > 0) {
      console.info(`Create page ${gatsbyUri}`);

      createPage({
        path: gatsbyUri,
        component: templateFile,
        context: {
          id,
          language,
          type: type.handle,
        },
      });
    } else {
      console.warn(
        `Skipping ${gatsbyUri}, template file is missing: ${templateFile}`,
      );
    }
  });
};

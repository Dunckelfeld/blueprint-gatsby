const path = require('path');

module.exports = ({ actions }) => {
  // Set custom paths
  actions.setWebpackConfig({
    resolve: {
      alias: {
        '~': path.resolve(__dirname, '../src/'),
        components: path.resolve(__dirname, '../src/components/'),
        utils: path.resolve(__dirname, '../src/utils/'),
        'node-fetch$': 'node-fetch/lib/index.js', // https://github.com/bitinn/node-fetch/issues/493
      },
    },
  });
};

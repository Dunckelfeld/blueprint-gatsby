export const colors = {
  black: '#000000',
  white: '#ffffff',
  error: '#EF4C4C',
  success: '#24AA62',
};

export const sizes = {
  spacing: 24,
};

export const grid = {
  gutter: 30,
  width: {
    xs: '100%',
    sm: 540,
    md: 720,
    lg: 960,
    xl: 1140,
  },
};

export const breakpoints = {
  xs: 0,
  sm: 576,
  md: 768,
  lg: 992,
  xl: 1200,
};

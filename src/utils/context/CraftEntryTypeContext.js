import React from "react";

/**
 * Context: entry type.
 *
 * Can be used to set the entry type that comes via pageContext in
 * the Layout component. With that you can check the entry type
 * that is displayed from anywhere you want.
 */
export default React.createContext();

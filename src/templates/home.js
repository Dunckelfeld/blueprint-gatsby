import React from 'react';
import { graphql } from 'gatsby';

/**
 * Template: Homepage
 *
 * Use this template in correspondence with a craft
 * section with the following properties:
 *
 * Type: single
 * Name: home
 * URI: keep the uri empty, because it's the home page
 */
export default function TemplateHome({ data, pageContext }) {
  return (
    <div>
      Home is where the template says so.{' '}
      <span role="img" aria-label="home">
        🏡
      </span>
    </div>
  );
}

export const query = graphql`
  query QueryHome($id: [Int]!) {
    craft {
      entry(id: $id) {
        id
        title
        # ... on Craft_Home {
        # Define home related fields here.
        # E.g. the page builder used on that page.
        # pageBuilderNeo {
        #   ...PageBuilderQuery
        # }
        # }
      }
    }
  }
`;

import React from 'react';
import { graphql } from 'gatsby';

/**
 * Template: Pages
 *
 * Use this template in correspondence with a craft
 * section with the following properties:
 *
 * Type: channel
 * Name: posts
 * URI: {parent.uri}/{slug}
 *
 * And an entry type with the following properties:
 * Name: post
 */
export default function TemplatePosts({ data, pageContext }) {
  return (
    <div>
      Home is where the template says so.{' '}
      <span role="img" aria-label="home">
        🏡
      </span>
    </div>
  );
}

export const query = graphql`
  query QueryPost($id: [Int]!) {
    craft {
      entry(id: $id) {
        id
        title
        # ... on Craft_PostsPost {
        # Define home related fields here.
        # E.g. the page builder used on that page.
        # pageBuilderNeo {
        #   ...PageBuilderQuery
        # }
        # }
      }
    }
  }
`;

import React from 'react';
import { graphql } from 'gatsby';

/**
 * Template: Error 404
 *
 * Use this template in correspondence with a craft
 * section with the following properties:
 *
 * Type: single
 * Name: error404
 * URI: 404
 */
export default function TemplateError404({ data, pageContext }) {
  return (
    <div>
      Error 404, page not found{' '}
      <span role="img" aria-label="home">
        😨
      </span>
    </div>
  );
}

export const query = graphql`
  query QueryError404($id: [Int]!) {
    craft {
      entry(id: $id) {
        id
        title
        # ... on Craft_Error404 {
        # Define error 404 related fields here.
        # E.g. the page builder used on that page.
        # pageBuilderNeo {
        #   ...PageBuilderQuery
        # }
        # }
      }
    }
  }
`;

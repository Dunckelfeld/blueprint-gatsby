import React from 'react';
import { Global } from '@emotion/core';

import { globalStyles, WrapperStyled } from './LayoutStyled';

export default function Layout({ children, pageContext }) {
  console.log(`Showing entry of type ${pageContext?.type}`);

  return (
    <WrapperStyled>
      <Global styles={globalStyles} />
      <div>{children}</div>
    </WrapperStyled>
  );
}

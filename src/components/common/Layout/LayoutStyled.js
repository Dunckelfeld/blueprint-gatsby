import { css } from '@emotion/core';
import styled from '@emotion/styled';

export const globalStyles = css({
  html: {
    backgroundColor: 'pink',
  },
});

export const WrapperStyled = styled('div')({
  backgroundColor: 'hotpink',
  padding: 20,
});

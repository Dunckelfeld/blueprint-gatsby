import React from 'react';
import { StaticQuery, graphql } from 'gatsby';

function ExampleComponentWithQueryData({ data, ...rest }) {
  return '';
}

export default props => (
  <StaticQuery
    query={graphql`
      query ExampleComponentQuery {
        craft {
          # some static query
        }
      }
    `}
    render={data => <ExampleComponentWithQueryData data={data} {...props} />}
  />
);

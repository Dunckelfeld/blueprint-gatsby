import styled from 'react-emotion';

export const ParentStyled = styled('div')({}, props => ({
  someAttribute: props.someprop,
}));

export const ChildStyled = styled('div')({}, props => ({
  someAttribute: props.someprop,
}));

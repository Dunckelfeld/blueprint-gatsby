/**
 * This example shows the usage of a styled functional component.
 *
 * ~ Robine 30.01.19
 */

/**
 * Library imports
 */
import React, { useState } from 'react';
import PropTypes from 'prop-types';

/**
 * Local imports
 */
import { someGreatFunction } from 'utils/somegreatutil';

/**
 * Relative imports
 */
import {
  ParentStyled,
  ChildStyled,
} from './ExampleComponentFunctionalStyled.js';

/**
 * Use a normal function declaration.
 * Export the component as default.
 *
 * @param {*} props
 */
export default function ExampleComponentFunctional({ article, isOnFire }) {
  /**
   * You can do simple derived values from props here.
   * If things get very heavy consider memoization
   * (React.useMemo, React.memo, memoize-one
   * [not currently in the codebase]).
   *
   * For simple deep lookups use optional-chaining.
   */
  const headline = article?.headline;

  /**
   * We want to use functional compononets only,
   * therefore we useState, useEffect ect.
   *
   * @see https://reactjs.org/docs/hooks-intro.html
   */
  const [count, setCount] = useState(0);

  /**
   * Return rendered node.
   * If a wrapper html-element is not imporant always use Fragment.
   */
  return (
    <ParentStyled>
      {headline}
      <ChildStyled>
        {/*
         * If you need conditionals use ternary syntax.
         * "bool &&" is also okay, if you make sure bool is in fact a boolean.
         * => use Boolean() to cast
         * => use comparison for numbers: count > 0
         */}
        {isOnFire ? <span>🔥🔥</span> : <span>🚰</span>}

        {/**
         * Here, article could be an object or null, or anything in that
         * sense, because it is an external prop.
         * If it would contain `""` or `0`, react would render that instead
         * of nothing.
         */}
        {Boolean(article) && (
          <>
            <p>You clicked the book {count} times</p>
            <button onClick={() => setCount(count + 1)}>📖</button>
          </>
        )}
      </ChildStyled>
    </ParentStyled>
  );
}

/**
 * Define prop types for every used prop.
 */
ExampleComponentFunctional.propTypes = {
  isOnFire: PropTypes.bool,
  article: PropTypes.object.isRequired,
};

/**
 * Every non-required prop should have a default.
 */
ExampleComponentFunctional.defaultProps = {
  isOnFire: true,
};

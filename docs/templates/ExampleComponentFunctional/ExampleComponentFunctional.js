import React, { useState } from 'react';
import PropTypes from 'prop-types';

import { someGreatFunction } from 'utils/somegreatutil';

import {
  ParentStyled,
  ChildStyled,
} from './ExampleComponentFunctionalStyled.js';

export default function ExampleComponentFunctional({ article, isOnFire }) {
  const headline = article?.headline;
  const [count, setCount] = useState(0);

  return (
    <ParentStyled>
      {headline}
      <ChildStyled>
        {isOnFire ? <span>🔥🔥</span> : <span>🚰</span>}

        {Boolean(article) && (
          <>
            <p>You clicked the book {count} times</p>
            <button onClick={() => setCount(count + 1)}>📖</button>
          </>
        )}
      </ChildStyled>
    </ParentStyled>
  );
}

ExampleComponentFunctional.propTypes = {
  isOnFire: PropTypes.bool,
  article: PropTypes.object.isRequired,
};

ExampleComponentFunctional.defaultProps = {
  isOnFire: true,
};

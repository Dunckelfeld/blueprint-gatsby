/**
 * Only ever export the main component as default
 */
export { default } from './ExampleComponentFunctional';

const path = require('path');
require('dotenv').config({ path: '.env' });

module.exports = {
  siteMetadata: {
    title: 'Gatsby blueprint with Craft support',
    siteUrl: 'https://thenextgreatproject.io', // No trailing slash (used to create xml sitemap)
  },
  pathPrefix: '/',
  plugins: [
    {
      resolve: 'gatsby-plugin-manifest',
      options: {
        name: 'Gatsby blueprint with Craft support',
        short_name: 'CraBy 🦀',
        start_url: '/',
        background_color: '#ffffff',
        theme_color: '#c0c0c0',
        // Enables "Add to Homescreen" prompt and disables browser UI (including back button)
        // see https://developers.google.com/web/fundamentals/web-app-manifest/#display
        display: 'minimal-ui',
        icon: 'src/assets/img/favicon.png', // This path is relative to the root of the site.
        include_favicon: true, // Include favicon
      },
    },
    'gatsby-plugin-react-helmet',
    'gatsby-plugin-postcss',
    {
      resolve: 'gatsby-plugin-layout',
      options: {
        component: require.resolve('./src/components/common/Layout/Layout.js'),
      },
    },
    {
      resolve: 'gatsby-source-graphql',
      options: {
        fieldName: 'craft',
        typeName: 'Craft',
        url: process.env.API_URL,
        headers: {
          Authorization: process.env.API_TOKEN,
        },
        refetchInterval: 120,
      },
    },
    {
      resolve: 'gatsby-plugin-emotion',
      options: {
        // Accepts all options defined by `babel-plugin-emotion` plugin.
        autoLabel: true,
        labelFormat: '[filename]--[local]',
      },
    },
    {
      resolve: 'gatsby-plugin-sitemap',
      options: {
        query: `
          {
            site {
              siteMetadata {
                siteUrl
              }
            }

            allSitePage {
              edges {
                node {
                  path
                }
              }
            }
        }`,
        serialize: ({ site, allSitePage }) =>
          allSitePage.edges.map(edge => ({
            url: site.siteMetadata.siteUrl + edge.node.path,
            changefreq: 'weekly',
            priority: 0.5,
          })),
      },
    },
    {
      resolve: 'gatsby-plugin-polyfill-io',
      options: {
        features: [
          'WeakSet',
          'String.prototype.startsWith',
          'Array.prototype.includes',
          'String.prototype.endsWith',
          'Array.from',
          'IntersectionObserver',
        ],
      },
    },
  ],
};

# thenextgreatproject 🧚🏻‍

## Develop

1. Copy `.env.example`, rename it to `.env`
2. Adjust `.env` with needed information
3. `yarn install`
4. `yarn develop`

You can now go to https://localhost:8000 and https://localhost:8000/___graphql

### Example component

You can find an example component in `docs/template/`

## Test

@todo

## Build

1. `yarn build`
2. `yarn serve`

You can now go to https://localhost:9000

## Deploy

@todo
